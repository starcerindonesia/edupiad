@extends('layouts.LayoutLogin')
@section('content')
<main>
    <div id="primary" style="background-color:#16262e" class="height-full responsive-phone">
        <div class="container">
            <div class="row">

                <div class="col-lg-6" style="background-color:#16262e">
                  <br/>
                  <br/>
                  <br/>
                  <br/>
                  <br/><br/><br/><br>
                  <img  src="/img/Edupiad.png" height="200px">
              </div>
              <div class="col-lg-6 p-t-100">
                <div class="text-white">
                    <h1>Selamat datang Kembali Di Edupiad.co.id</h1>
                    <p class="s-18 p-t-b-20 font-weight">Silahkan Login Untuk Masuk System</p>
                </div>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group has-icon"><i class="fa fa-user fa-3x"></i>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Masukan Email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group has-icon"><i class="fa fa-lock fa-3x"></i>
                              <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Masukan Password" name="password" required>

                              @if ($errors->has('password'))
                              <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-12">

                        <button type="submit" class="btn bg-yellow btn-lg btn-block">
                            {{ __('Login') }}
                        </button>

                    </div>
                   


                </div>
                 <div class="row">
                        <div class="col"> 
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                <p>
                               Lupa Kata Sandi?
                            </a>
                        
                        </div>
                        <div class="col">
                            <p>
                            <div align="right">Belum pnya aku silahkan <a class="btn btn-link" href="/register" >Register</a></div>
                        </div>
                    </div>
            </form>



            <br>
            <br>
            <br>
            <br>
            <br>

            <div class="col-lg-12">
              <div class="footer">
               <u><b>Edupiad.co.id &copy;<?php $tanggal = getdate(); echo $tanggal['year'];?></b></u>
           </div>
       </div>
   </div>
</form>
</div>
</div>
</div>
</main>
@endsection
