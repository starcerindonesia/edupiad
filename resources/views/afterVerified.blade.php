@extends('layouts.verifikasi')
@section('content')
<a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            <div class="contact-image">
                <img src="/theme/assets/images/LogoEdupiad.jpg" alt="rocket_contact"/>
            </div>
            <form method="put" action="user/{{Session::get('email')}}">
                <h3>Mohon Isi Data Diri Anda</h3>
               <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="Nis" class="form-control" placeholder="Masukan Nis *" value=""  required/>
                        </div>
                        <div class="form-group">
                            <input type="text" name="No_HP" class="form-control" placeholder="Masukan Nomer HP *" value="" required />
                        </div>
                        <div class="form-group">
                            <input type="date" name="Tanggal_lahir" class="form-control" placeholder="Masukan Tanggal Lahir *" value="" required />
                        </div>

                    </div>
                    <div class="col-md-6">
					<div class="form-group">
                            <input type="text" name="Tempat_lahir" class="form-control" placeholder="Masukan Tempat lahir *" value="" />
                    </div>
					<div class="form-group">
					<select class="form-control" required>
						<option class="form-control">Pilih Ketegori</option>
						<option class="form-control">Guru</option>
						<option class="form-control">Siswa</option>

					</select>
                    </div>
                        <div class="form-group">
                            <input type="submit" name="btnSubmit" class="btnContact" value="Verifikasi" />
                        </div>
                    </div>
                </div>
            </form>
</div>

				@endsection
