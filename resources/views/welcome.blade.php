<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Edupiad - Edukasi Olimpiade Indonesia</title>
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<link rel="stylesheet" href="/theme/assets/css/style_login.css">

</head>
<body>
	<div class="login-reg-panel">
		<div class="login-info-box">
			<h2>Sudah Punya Akun?</h2>
			<p>Pastikan Sudah Verifikasi E-Mail</p>
			<label id="label-register" for="log-reg-show">Login</label>
			<input type="radio" name="active-log-panel" id="log-reg-show" class="button button3"  checked="checked">
		</div>

		<div class="register-info-box">
			<h2>Belum Punya Akun?</h2>
			<p>Silahkan Daftar Terlebih Dahulu</p>
			<label id="label-login" for="log-login-show">Daftar</label>
			<input type="radio" name="active-log-panel" id="log-login-show">
		</div>

		<div class="white-panel">
			<div class="login-show">
				<h2>LOGIN</h2>
				<form method="POST" action="{{ route('login') }}">
					@csrf
					<input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
					{{-- Jika Ada Kesalahan login --}}
					@if ($errors->has('email'))
					<span class="invalid-feedback" role="alert" style="color: red">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
					@endif
					<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
					{{-- Jika Ada Kesalahan --}}
					@if ($errors->has('password'))
					<span class="invalid-feedback" role="alert" style="color: red">
						<strong>{{ $errors->first('password') }}</strong>
					</span>
					@endif
					<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

					<label class="form-check-label" for="remember">
						{{ __('Remember Me') }}
					</label>
					<button type="submit" class="button button3">Login</button>
				</form>
				@if (Route::has('password.request'))
				<a class="btn btn-link" style="color: black" href="{{ route('password.request') }}">
					{{ __('Lupa Password kamu?') }}
				</a>
				@endif
			</div>
			<div class="register-show">
				<h2>REGISTER</h2>
				<form method="POST" action="{{ route('register') }}">
					@csrf
					<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Nama Lengkap">

					@if ($errors->has('name'))
					<span class="invalid-feedback" role="alert" style="color: red">
						<strong>{{ $errors->first('name') }}</strong>
					</span>
					@endif

					<input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">

					@if ($errors->has('email'))
					<span class="invalid-feedback" role="alert" style="color: red">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
					@endif

					<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">

					@if ($errors->has('password'))
					<span class="invalid-feedback" role="alert" style="color: red">
						<strong>{{ $errors->first('password') }}</strong>
					</span>
					@endif
					 <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Ketik Ulang Password">
					<button type="submit"class="button button3">Daftar Sekarang</button>
				</form>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function(){
			$('.login-info-box').fadeOut();
			$('.login-show').addClass('show-log-panel');
		});


		$('.login-reg-panel input[type="radio"]').on('change', function() {
			if($('#log-login-show').is(':checked')) {
				$('.register-info-box').fadeOut();
				$('.login-info-box').fadeIn();

				$('.white-panel').addClass('right-log');
				$('.register-show').addClass('show-log-panel');
				$('.login-show').removeClass('show-log-panel');

			}
			else if($('#log-reg-show').is(':checked')) {
				$('.register-info-box').fadeIn();
				$('.login-info-box').fadeOut();

				$('.white-panel').removeClass('right-log');

				$('.login-show').addClass('show-log-panel');
				$('.register-show').removeClass('show-log-panel');
			}
		});
	</script>
</body>
</html>
