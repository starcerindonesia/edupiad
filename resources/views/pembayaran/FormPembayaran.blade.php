@extends('layouts.MasterDasboard')

@section('content')

				<!-- Page header -->
	<form action="#" method="post">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Konfirmasi </span>Pembayaran</h4>
						<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="form_floating_labels.html">Forms</a></li>
							<li class="active">Konfirmasi Pembayaran</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Floating labels -->
					<div class="row">
						<div class="col-md-6">

							<!-- Title -->
							<h6 class="content-group text-semibold">
								Biodata Pembayaran
								<small class="display-block">Input Biodata</small>
							</h6>
							<!-- /title -->


							<!-- Text inputs -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Biodata Pembayaran<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
									<div class="form-group form-group-material">
										<label class="control-label is-visible">Atas nama penyetor/Rekening Atas Nama <font color="red">*</font></label>
										<input type="text" class="form-control" placeholder="Atas nama penyetor/Rekening Atas Nama">
									</div>

									<div class="form-group form-group-material">
										<label class="control-label is-visible">Nomer Rekening</label>
										<input type="password" class="form-control" placeholder="Nomer Rekening">
									</div>

									<div class="form-group form-group-material">
										<label class="control-label is-visible">Nomer Pendaftaran/Order</label>
										<input type="text" class="form-control" placeholder="Nomer Pendaftaran/Order">
									</div>

									<div class="form-group form-group-material">
										<label class="control-label is-visible">No Telepon</label>
										<input type="number" class="form-control" placeholder="No Telepon">
									</div>

									
								</div>
							</div>

						</div>

						<div class="col-md-6">

							<!-- Title -->
							<h6 class="content-group text-semibold">
								Data Pembayaran
								<small class="display-block">Input Data Pembayaran</small>
							</h6>
							<!-- /title -->


							<!-- Text inputs -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Data Pembayaran<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
									<div class="form-group form-group-material">
										<label class="control-label is-visible">Tanggal Pembyaran</label>
										<input type="date" class="form-control" value="Value" placeholder="Text input">
									</div>

									<div class="form-group form-group-material">
										<label class="control-label is-visible">Rekening Tujuan</label>
										<select name="select" class="form-control">
			                                    <option value="#">Pilih No Rekening Tujuan</option>
			                                    <option value="opt2">BRI - 1234567 a/n Edukasi Olimpiade Indonesia</option>
			                                    <option value="opt3">Mandiri - 1234567 a/n Edukasi Olimpiade Indonesia</option>
			                                    <option value="opt4">BCA - 1234567 a/n Edukasi Olimpiade Indonesia</option>
			                                    <option value="opt5">OCBC - 1234567 a/n Edukasi Olimpiade Indonesia</option>
			                                </select>
										</div>

									<div class="form-group form-group-material">
										<label class="control-label is-visible">Total Pembayaran</label>
										<input type="number" class="form-control" value="Total Pembayaran" placeholder="Total Pembayaran">
									</div>
									<div class="form-group form-group-material">
									<label class="control-label is-visible">Bukti Pembayaran</label>
									<div class="uploader">
									<input type="file" class="file-styled"><span class="filename" style="user-select: none;">No file selected</span><span class="action btn bg-pink-400" style="user-select: none;">Choose File</span></div>
									</div>
							</div>
							
						</div>
						
						</div>

						<div class="col-md-12">
						<input type="submit" class="btn btn-primary btn-block" name="simpan" value="Konirmasi Sekarang">
						</div>
					<!-- /floating labels -->

				</form>


					<!-- Footer -->
					<div class="footer text-muted">
						© 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>


@endsection
