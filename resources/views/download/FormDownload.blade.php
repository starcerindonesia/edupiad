@extends('layouts.MasterDasboard')

@section('content')

<!-- Page header -->
<form action="#" method="post">
	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Download </span>Materi</h4>
				<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

				<div class="heading-elements">
					<div class="heading-btn-group">
						<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
						<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
						<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
					</div>
				</div>
			</div>

			<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
				<ul class="breadcrumb">
					<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
					<li><a href="form_floating_labels.html">Forms</a></li>
					<li class="active">Download Materi</li>
				</ul>

				<ul class="breadcrumb-elements">
					<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-gear position-left"></i>
							Settings
							<span class="caret"></span>
						</a>

						<ul class="dropdown-menu dropdown-menu-right">
							<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
							<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
							<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
							<li class="divider"></li>
							<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<!-- /page header -->


		<!-- Content area -->
		<div class="content">

			<!-- Floating labels -->
			<div class="row">
				<div class="col-md-12">
					<!-- Text inputs -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Download Materi<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
							<div class="heading-elements">
								<ul class="icons-list">
									<li><a data-action="collapse"></a></li>
									<li><a data-action="reload"></a></li>
									<li><a data-action="close"></a></li>
								</ul>
							</div>
						</div>

						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<div class="input-group" id="adv-search">
										<div class="row">
											<div class="col-sm-1">  <button type="button" class="btn btn-primary btn-block">+</button>
											</div>
											<div class="col-sm-11"><input type="text" class="form-control" placeholder="Pencarian Program" />
											</div>

										</div>
										<div class="input-group-btn">
											<div class="btn-group" role="group">
												<div class="dropdown dropdown-lg">
													<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
												</div>
											</div>
											
										</div>
										
									</div>
									<table class="table table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th class="hidden-sm">Nama File</th>
												<th>Type</th>
												<th>Size</th>
												<th>Hits</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td class="text-nowrap">
													<h5><a href="javascript:;">TATA TERTIB &amp; TEMPAT TES PRA OSK 2019 SE JAWA TENGAH</a></h5>
												</td>
												<td><label class="label label-warning">.PDF</label></td>
												<td><label class="label label-warning">68.36KB</label></td>

												<td></td>
												<td class="text-nowrap">

													<a href="#" class="btn btn-primary"><i class="fa fa-download"></i> UNDUH</a>											
												</td>
											</tr>
											<tr>
												<td>2</td>
												<td class="text-nowrap">
													<h5><a href="javascript:;">TATA TERTIB &amp; TEMPAT TES PRA OSK 2019 SE JAWA TENGAH</a></h5>
												</td>
												<td><label class="label label-warning">.PDF</label></td>
												<td><label class="label label-warning">68.36KB</label></td>

												<td></td>
												<td class="text-nowrap">

													<a href="#" class="btn btn-primary"><i class="fa fa-download"></i> UNDUH</a>											
												</td>
											</tr>


										</tbody>
									</table>
								</div>

							</div>

							<!-- /floating labels -->

						</form>



					</div>
					<!-- /content area -->

				</div>


				@endsection
