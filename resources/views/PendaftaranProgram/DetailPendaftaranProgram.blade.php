@extends('layouts.MasterDasboard')

@section('content')
	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Detail </span>Program</h4>
				<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

				<div class="heading-elements">
					<div class="heading-btn-group">
						<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
						<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
						<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
					</div>
				</div>
			</div>

			<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
				<ul class="breadcrumb">
					<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
					<li class="active">Detail Program </li>
				</ul>

				<ul class="breadcrumb-elements">
					<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-gear position-left"></i>
							Settings
							<span class="caret"></span>
						</a>

						<ul class="dropdown-menu dropdown-menu-right">
							<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
							<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
							<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
							<li class="divider"></li>
							<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<!-- /page header -->


		<!-- Content area -->
		<div class="content">
			<!-- Detached content -->
			<div class="scroll">
			<div class="container-detached">
					<div class="content-detached">
					@method('PATCH')
					@csrf
					<!-- Course overview -->
					<div class="panel panel-white">
						<div class="panel-heading">
							<h6 class="panel-title text-semibold">{{ $program->nama_program }}</h6>

							<!-- <div class="heading-elements">
								<ul class="list-inline list-inline-separate heading-text">
									<li>Rating: <span class="text-semibold">4.85</span></li>
									<li>
										<i class="icon-star-full2 text-size-base text-warning-300"></i>
										<i class="icon-star-full2 text-size-base text-warning-300"></i>
										<i class="icon-star-full2 text-size-base text-warning-300"></i>
										<i class="icon-star-full2 text-size-base text-warning-300"></i>
										<i class="icon-star-full2 text-size-base text-warning-300"></i>
										<span class="text-muted position-right">(439)</span>
									</li>
								</ul>
							</div>-->
						</div>

						<ul class="nav nav-lg nav-tabs nav-tabs-bottom nav-tabs-toolbar no-margin">

							 	@if($kategori=="guru")
							 <li class="active"><a href="#course-overview" data-toggle="tab"><i class="icon-menu7 position-left"></i> Pendaftaran</a></li>
							 <li class="active"><a href="#course-attendees" data-toggle="tab"><i class="icon-people position-left"></i> Data Peserta</a></li>
							 @else
							 @endif
							<li><a href="#course-schedule" data-toggle="tab"><i class="icon-calendar3 position-left"></i> Data Sekolah</a></li>
						</ul>

						<div class="tab-content">
							<div class="tab-pane fade in active" id="course-overview">
								<div class="panel-body">
									<div class="content-group-lg">
										<center>
										<img src="/img/gambar_program/{{$program->gambar}}" class="img-responsive img-rounded" alt="" >
										</center>
										<h6 class="text-semibold">Tempat</h6>
										<p>{{ $program->tempat_kegiatan }}</p>
									</div>
								</div>

								<div class="scroll">
									<table class="table table-striped" style="width: 810px">
										<thead>
											<tr>
												<th style="width: 25px">#</th>
												<th style="width: 200px">Nama Peserta</th>
												<th>L/P</th>
												<th style="width: 100px">Kelas</th>
												<th style="width: 120px">Bidang</th>
												<th style="width: 150px">Sekolah</th>
												<th style="width: 100px">Status</th>
												<th style="width: 190px">Aksi</th>
											</tr>
										</thead>
										<tbody>

											<tr>
												<td>1</td>
												<td><a href="#">Eri Cahyana</a></td>
												<td>L</td>
												<td>S1</td>
												<td>Teknologin Informatika</td>
												<td>LPKIA</td>
												<td><span class="label label-warning">Proses</span></td>
												<td>
													<div class="col-md-6">
													<a class="btn btn-primary ">Edit</a>
													</div>
													<div class="col-md-6">
													<a class="btn btn-danger">Hapus</a></td>
												</div>
											</tr>

										</tbody>
									</table>
								</div>
							</div>

							<div class="tab-pane fade" id="course-attendees">
								<div class="panel-body">
									<div class="row">
										@foreach($data_peserta as $dat_pers)
										<div class="col-md-6">
											<div class="panel panel-body">
												<div class="media">
													<div class="media-left">
														<a href="#">
															<img src="/img/{{$dat_pers->foto}}" class="img-circle img-lg" alt="">
														</a>
													</div>

													<div class="media-body">
														<h6 class="media-heading">{{$dat_pers->name}}</h6>
														<span class="text-muted">{{$dat_pers->kategori}}</span>
													</div>

													<div class="media-right media-middle">
														<ul class="icons-list">
																<li class="dropdown">
																	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
																		<ul class="dropdown-menu dropdown-menu-right">
																					<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
																					<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
																					<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
																					<li class="divider"></li>
																					<li><a href="#"><i class="icon-history pull-right"></i> History</a></li>
																	</ul>
																</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
											@endforeach
									</div>
									<div class="text-center content-group pt-20">
															<ul class="pagination">
											<li class="disabled"><a href="#">&larr;</a></li>
											<li class="active"><a href="#">1</a></li>
											<li><a href="#">2</a></li>
											<li><a href="#">3</a></li>
											<li><a href="#">4</a></li>
											<li><span>...</span></li>
											<li><a href="#">58</a></li>
											<li><a href="#">59</a></li>
											<li><a href="#">&rarr;</a></li>
										</ul>
									</div>
								</div>
							</div>

							<div class="tab-pane fade" id="course-schedule">
								<div class="panel-body">
									<div class="schedule"></div>
								</div>
							</div>
						</div>
					</div>
					<!-- /course overview -->


					<!-- About author -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h6 class="panel-title">About the author</h6>

							<div class="heading-elements">
								<ul class="icons-list icons-list-extended heading-text">
														<li><a href="#" data-popup="tooltip" data-container="body" title="Google Drive"><i class="icon-google-drive"></i></a></li>
														<li><a href="#" data-popup="tooltip" data-container="body" title="Twitter"><i class="icon-twitter"></i></a></li>
														<li><a href="#" data-popup="tooltip" data-container="body" title="Github"><i class="icon-github"></i></a></li>
														<li><a href="#" data-popup="tooltip" data-container="body" title="Linked In"><i class="icon-linkedin"></i></a></li>
													</ul>
							</div>
						</div>

						<div class="media panel-body no-margin">
							<div class="media-left">
								<a href="#">
									<img src="/theme/assets/images/placeholder.jpg" style="width: 68px; height: 68px;" class="img-circle" alt="">
								</a>
							</div>

							<div class="media-body">
								<h6 class="media-heading text-semibold">James Alexander</h6>
								<p>So slit more darn hey well wore submissive savage this shark aardvark fumed thoughtfully much drank when angelfish so outgrew some alas vigorously therefore warthog superb less oh groundhog less alas gibbered barked some hey despicably with aesthetic hamster jay by luckily</p>

								<ul class="list-inline list-inline-separate no-margin">
									<li><a href="#">Author profile</a></li>
									<li><a href="#">All courses by James</a></li>
									<li><a href="#">http://website.com</a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /about author -->


					<!-- Comments -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h6 class="panel-title text-semiold">Discussion</h6>
							<div class="heading-elements">
								<ul class="list-inline list-inline-separate heading-text text-muted">
									<li>42 comments</li>
									<li>75 pending review</li>
								</ul>
											</div>
						</div>

						<div class="panel-body">
							<ul class="media-list content-group-lg stack-media-on-mobile">
								<li class="media">
									<div class="media-left">
										<a href="#"><img src="/theme/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
									</div>

									<div class="media-body">
										<div class="media-heading">
											<a href="#" class="text-semibold">William Jennings</a>
											<span class="media-annotation dotted">Just now</span>
										</div>

										<p>He moonlight difficult engrossed an it sportsmen. Interested has all devonshire difficulty gay assistance joy. Unaffected at ye of compliment alteration to.</p>

										<ul class="list-inline list-inline-separate text-size-small">
											<li>114 <a href="#"><i class="icon-arrow-up22 text-success"></i></a><a href="#"><i class="icon-arrow-down22 text-danger"></i></a></li>
											<li><a href="#">Reply</a></li>
											<li><a href="#">Edit</a></li>
										</ul>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#"><img src="/theme/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
									</div>

									<div class="media-body">
										<div class="media-heading">
											<a href="#" class="text-semibold">Margo Baker</a>
											<span class="media-annotation dotted">5 minutes ago</span>
										</div>

										<p>Place voice no arise along to. Parlors waiting so against me no. Wishing calling are warrant settled was luckily. Express besides it present if at an opinion visitor.</p>

										<ul class="list-inline list-inline-separate text-size-small">
											<li>90 <a href="#"><i class="icon-arrow-up22 text-success"></i></a><a href="#"><i class="icon-arrow-down22 text-danger"></i></a></li>
											<li><a href="#">Reply</a></li>
											<li><a href="#">Edit</a></li>
										</ul>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#"><img src="/theme/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
									</div>

									<div class="media-body">
										<div class="media-heading">
											<a href="#" class="text-semibold">James Alexander</a>
											<span class="media-annotation dotted">7 minutes ago</span>
										</div>

										<p>Savings her pleased are several started females met. Short her not among being any. Thing of judge fruit charm views do. Miles mr an forty along as he.</p>

										<ul class="list-inline list-inline-separate text-size-small">
											<li>70 <a href="#"><i class="icon-arrow-up22 text-success"></i></a><a href="#"><i class="icon-arrow-down22 text-danger"></i></a></li>
											<li><a href="#">Reply</a></li>
											<li><a href="#">Edit</a></li>
										</ul>

										<div class="media">
											<div class="media-left">
												<a href="#"><img src="/theme/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
											</div>

											<div class="media-body">
												<div class="media-heading">
													<a href="#" class="text-semibold">Jack Cooper</a>
													<span class="media-annotation dotted">10 minutes ago</span>
												</div>

												<p>She education get middleton day agreement performed preserved unwilling. Do however as pleased offence outward beloved by present. By outward neither he so covered.</p>

												<ul class="list-inline list-inline-separate text-size-small">
													<li>67 <a href="#"><i class="icon-arrow-up22 text-success"></i></a><a href="#"><i class="icon-arrow-down22 text-danger"></i></a></li>
													<li><a href="#">Reply</a></li>
													<li><a href="#">Edit</a></li>
												</ul>

												<div class="media">
													<div class="media-left">
														<a href="#"><img src="/theme/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
													</div>

													<div class="media-body">
														<div class="media-heading">
															<a href="#" class="text-semibold">Natalie Wallace</a>
															<span class="media-annotation dotted">1 hour ago</span>
														</div>

														<p>Juvenile proposal betrayed he an informed weddings followed. Precaution day see imprudence sympathize principles. At full leaf give quit to in they up.</p>

														<ul class="list-inline list-inline-separate text-size-small">
															<li>54 <a href="#"><i class="icon-arrow-up22 text-success"></i></a><a href="#"><i class="icon-arrow-down22 text-danger"></i></a></li>
															<li><a href="#">Reply</a></li>
															<li><a href="#">Edit</a></li>
														</ul>
													</div>
												</div>

												<div class="media">
													<div class="media-left">
														<a href="#"><img src="/theme/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
													</div>

													<div class="media-body">
														<div class="media-heading">
															<a href="#" class="text-semibold">Nicolette Grey</a>
															<span class="media-annotation dotted">2 hours ago</span>
														</div>

														<p>Although moreover mistaken kindness me feelings do be marianne. Son over own nay with tell they cold upon are. Cordial village and settled she ability law herself.</p>

														<ul class="list-inline list-inline-separate text-size-small">
															<li>41 <a href="#"><i class="icon-arrow-up22 text-success"></i></a><a href="#"><i class="icon-arrow-down22 text-danger"></i></a></li>
															<li><a href="#">Reply</a></li>
															<li><a href="#">Edit</a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#"><img src="/theme/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
									</div>

									<div class="media-body">
										<div class="media-heading">
											<a href="#" class="text-semibold">Victoria Johnson</a>
											<span class="media-annotation dotted">3 hours ago</span>
										</div>

										<p>Finished why bringing but sir bachelor unpacked any thoughts. Unpleasing unsatiable particular inquietude did nor sir.</p>

										<ul class="list-inline list-inline-separate text-size-small">
											<li>32 <a href="#"><i class="icon-arrow-up22 text-success"></i></a><a href="#"><i class="icon-arrow-down22 text-danger"></i></a></li>
											<li><a href="#">Reply</a></li>
											<li><a href="#">Edit</a></li>
										</ul>
									</div>
								</li>
							</ul>
						</div>

						<hr class="no-margin">

						<div class="panel-body">
							<h6 class="no-margin-top content-group">Add comment</h6>
							<div class="content-group">
								<div id="add-comment">Get his declared appetite distance his together now families. Friends am himself at on norland it viewing. Suspected elsewhere you belonging continued commanded she...</div>
							</div>

							<div class="text-right">
								<button type="button" class="btn bg-blue"><i class="icon-plus22"></i> Add comment</button>
							</div>
						</div>
					</div>
					<!-- /comments -->

			<!-- /detached content -->
		</div>

				<!-- Detached sidebar -->
				<div class="sidebar-detached">
					<div class="sidebar sidebar-default sidebar-separate">
						<div class="sidebar-content">

							<!-- Course details -->
							<div class="sidebar-category">
								<div class="category-title">
									<span>Detail Program</span>
									<ul class="icons-list">
										<li><a href="#" data-action="collapse"></a></li>
									</ul>
								</div>

								<div class="category-content">
									<a href="#" class="btn bg-teal-400 btn-block content-group">Konfirmasi Pendaftaran <i class="icon-check"></i></a>

									<div class="form-group">
										<label class="control-label no-margin text-semibold">Batas Pendaftaran:</label>
										<div class="pull-right">{!! date('d/m/Y',strtotime($program->batas_pendaftaran)) !!}</div>
									</div>

									<div class="form-group">
										<label class="control-label no-margin text-semibold">Status:</label>
										<div class="pull-right"><span class="label bg-blue">Registration</span></div>
									</div>

									<div class="form-group">
										<label class="control-label no-margin text-semibold">Tanggal Mulai:</label>
										<div class="pull-right">{!! date('F d, Y',strtotime($program->tanggal_awal)) !!}</div>
									</div>

									<div class="form-group">
										<label class="control-label no-margin text-semibold">Tanggal Akhir:</label>
										<div class="pull-right">{!! date('F d, Y',strtotime($program->tanggal_akhir)) !!}</div>
									</div>

									<div class="form-group">
										<label class="control-label no-margin text-semibold">Pemateri:</label>
										<div class="pull-right">{{$program->pemateri}}</div>
									</div>

									<div class="form-group">
										<label class="control-label no-margin text-semibold">Kehardiran:</label>
										<div class="pull-right">382 Orang</div>
									</div>
								</div>
							</div>
							<!-- /course details -->


							<!-- Categories -->
							<div class="sidebar-category">
								<div class="category-title">
									<span>Categories</span>
									<ul class="icons-list">
										<li><a href="#" data-action="collapse"></a></li>
									</ul>
								</div>

								<div class="category-content no-padding">
									<ul class="navigation navigation-alt navigation-accordion">
										<li class="navigation-header">Development</li>
										<li><a href="#"><span class="badge badge-default">37</span> Frontend development</a></li>
										<li><a href="#"><span class="badge badge-default">58</span> Backend development</a></li>
										<li><a href="#"><span class="badge badge-default">39</span> Engineering</a></li>
										<li class="navigation-header">Design</li>
										<li><a href="#"><span class="badge badge-default">21</span> Interface design</a></li>
										<li><a href="#"><span class="badge badge-default">10</span> User experience</a></li>
										<li><a href="#"><span class="badge badge-default">26</span> Web design</a></li>
									</ul>
								</div>
							</div>
							<!-- /categories -->


							<!-- Attendees -->
							<div class="sidebar-category">
								<div class="category-title">
									<span>Kehadiran Terbaru</span>
									<ul class="icons-list">
										<li><a href="#" data-action="collapse"></a></li>
									</ul>
								</div>

								<div class="category-content no-padding">
									<ul class="media-list media-list-linked">
										@if(!isset($pendaftar))
										@foreach($pendaftar as $pendaf)
										<li class="media">
											<a href="#" class="media-link">
												<div class="media-left">
													<img src="/img/{{$dat_pers->foto}}" class="img-sm img-circle" alt="">
												</div>

												<div class="media-body">
													<span class="media-heading text-semibold">{{$pendaf->name}}</span>
													<span class="text-size-mini text-muted display-block">{{$pendaf->kategori}}</span>
												</div>

												<div class="media-right media-middle">
													<span class="status-mark bg-success"></span>
												</div>
											</a>
										</li>
										@endforeach
										@else
											<li class="media">
												<center>
														<span class="media-heading text-semibold">Belum ada yang daftar</span>
												</center>
											</li>
											@endif
									</ul>
								</div>
							</div>
							<!-- /attendees -->


							<!-- Our trainers -->
							<div class="sidebar-category">
								<div class="category-title">
									<span>Our trainers</span>
									<ul class="icons-list">
										<li><a href="#" data-action="collapse"></a></li>
									</ul>
								</div>

								<div class="category-content">
									<ul class="media-list">
										<li class="media">
											<a href="#" class="media-left">
												<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="img-sm img-circle" alt="">
											</a>

											<div class="media-body">
												<a href="#">James Alexander</a>
												<span class="text-size-small text-muted display-block">Web development</span>
											</div>

											<div class="media-right media-middle">
												<ul class="icons-list text-nowrap">
													<li>
														<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
														<ul class="dropdown-menu dropdown-menu-right">
															<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Ask question</a></li>
															<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
															<li class="divider"></li>
															<li><a href="#"><i class="icon-stars pull-right"></i> Rate trainer</a></li>
														</ul>
													</li>
												</ul>
											</div>
										</li>

										<li class="media">
											<a href="#" class="media-left">
												<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="img-sm img-circle" alt="">
											</a>

											<div class="media-body">
												<a href="#">Jeremy Victorino</a>
												<span class="text-size-small text-muted display-block">Business</span>
											</div>

											<div class="media-right media-middle">
												<ul class="icons-list text-nowrap">
													<li>
														<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
														<ul class="dropdown-menu dropdown-menu-right">
															<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Ask question</a></li>
															<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
															<li class="divider"></li>
															<li><a href="#"><i class="icon-stars pull-right"></i> Rate trainer</a></li>
														</ul>
													</li>
												</ul>
											</div>
										</li>

										<li class="media">
											<a href="#" class="media-left">
												<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="img-sm img-circle" alt="">
											</a>

											<div class="media-body">
												<a href="#">Margo Baker</a>
												<span class="text-size-small text-muted display-block">Marketing</span>
											</div>

											<div class="media-right media-middle">
												<ul class="icons-list text-nowrap">
													<li>
														<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
														<ul class="dropdown-menu dropdown-menu-right">
															<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Ask question</a></li>
															<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
															<li class="divider"></li>
															<li><a href="#"><i class="icon-stars pull-right"></i> Rate trainer</a></li>
														</ul>
													</li>
												</ul>
											</div>
										</li>

										<li class="media">
											<a href="#" class="media-left">
												<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="img-sm img-circle" alt="">
											</a>

											<div class="media-body">
												<a href="#">Beatrix Diaz</a>
												<span class="text-size-small text-muted display-block">Human relations</span>
											</div>

											<div class="media-right media-middle">
												<ul class="icons-list text-nowrap">
													<li>
														<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
														<ul class="dropdown-menu dropdown-menu-right">
															<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Ask question</a></li>
															<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
															<li class="divider"></li>
															<li><a href="#"><i class="icon-stars pull-right"></i> Rate trainer</a></li>
														</ul>
													</li>
												</ul>
											</div>
										</li>

										<li class="media">
											<a href="#" class="media-left">
												<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="img-sm img-circle" alt="">
											</a>

											<div class="media-body">
												<a href="#">Richard Vango</a>
												<span class="text-size-small text-muted display-block">Server management</span>
											</div>

											<div class="media-right media-middle">
												<ul class="icons-list text-nowrap">
													<li>
														<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
														<ul class="dropdown-menu dropdown-menu-right">
															<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Ask question</a></li>
															<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
															<li class="divider"></li>
															<li><a href="#"><i class="icon-stars pull-right"></i> Rate trainer</a></li>
														</ul>
													</li>
												</ul>
											</div>
										</li>

										<li class="media">
											<a href="#" class="media-left">
												<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="img-sm img-circle" alt="">
											</a>

											<div class="media-body">
												<a href="#">Dave Raters</a>
												<span class="text-size-small text-muted display-block">Finances</span>
											</div>

											<div class="media-right media-middle">
												<ul class="icons-list text-nowrap">
													<li>
														<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
														<ul class="dropdown-menu dropdown-menu-right">
															<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Ask question</a></li>
															<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
															<li class="divider"></li>
															<li><a href="#"><i class="icon-stars pull-right"></i> Rate trainer</a></li>
														</ul>
													</li>
												</ul>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- /our trainers -->


							<!-- Upcoming courses -->
							<div class="sidebar-category">
								<div class="category-title">
									<span>Upcoming courses</span>
									<ul class="icons-list">
										<li><a href="#" data-action="collapse"></a></li>
									</ul>
								</div>

								<div class="category-content">
									<ul class="media-list">
										<li class="media">
											<div class="media-left">
												<h5 class="no-margin text-center text-success">
													02
													<small class="display-block text-size-small no-margin">Nov</small>
												</h5>
											</div>

											<div class="media-body">
												<span class="text-semibold">Web development strategy</span> course by <a href="#">James Franko</a>
												<ul class="list-inline list-inline-separate no-margin-bottom mt-5">
													<li><span class="text-muted">120 hours</span></li>
													<li>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-half text-size-base text-warning-300"></i>
													</li>
												</ul>
											</div>
										</li>

										<li class="media">
											<div class="media-left">
												<h5 class="no-margin text-center text-success">
													15
													<small class="display-block text-size-small no-margin">Nov</small>
												</h5>
											</div>

											<div class="media-body">
												<span class="text-semibold">Business development</span> course by <a href="#">Jeremy Victorino</a>
												<ul class="list-inline list-inline-separate no-margin-bottom mt-5">
													<li><span class="text-muted">40 hours</span></li>
													<li>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-half text-size-base text-warning-300"></i>
													</li>
												</ul>
											</div>
										</li>

										<li class="media">
											<div class="media-left">
												<h5 class="no-margin text-center text-success">
													01
													<small class="display-block text-size-small no-margin">Dec</small>
												</h5>
											</div>

											<div class="media-body">
												<span class="text-semibold">Digital marketing</span> course by <a href="#">Monica Smith</a>
												<ul class="list-inline list-inline-separate no-margin-bottom mt-5">
													<li><span class="text-muted">100 hours</span></li>
													<li>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
													</li>
												</ul>
											</div>
										</li>

										<li class="media">
											<div class="media-left">
												<h5 class="no-margin text-center text-success">
													15
													<small class="display-block text-size-small no-margin">Dec</small>
												</h5>
											</div>

											<div class="media-body">
												<span class="text-semibold">User experience design</span> course by <a href="#">Buzz Brenson</a>
												<ul class="list-inline list-inline-separate no-margin-bottom mt-5">
													<li><span class="text-muted">90 hours</span></li>
													<li>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-empty3 text-size-base text-warning-300"></i>
														<i class="icon-star-half text-size-base text-warning-300"></i>
													</li>
												</ul>
											</div>
										</li>

										<li class="media">
											<div class="media-left">
												<h5 class="no-margin text-center text-success">
													20
													<small class="display-block text-size-small no-margin">Dec</small>
												</h5>
											</div>

											<div class="media-body">
												<span class="text-semibold">Digital painting</span> course by <a href="#">Zachary Willson</a>
												<ul class="list-inline list-inline-separate no-margin-bottom mt-5">
													<li><span class="text-muted">120 hours</span></li>
													<li>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
													</li>
												</ul>
											</div>
										</li>

										<li class="media">
											<div class="media-left">
												<h5 class="no-margin text-center text-success">
													04
													<small class="display-block text-size-small no-margin">Jan</small>
												</h5>
											</div>

											<div class="media-body">
												<span class="text-semibold">Financial analysis</span> course by <a href="#">Dori Laperriere</a>
												<ul class="list-inline list-inline-separate no-margin-bottom mt-5">
													<li><span class="text-muted">60 hours</span></li>
													<li>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
														<i class="icon-star-full2 text-size-base text-warning-300"></i>
													</li>
												</ul>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- /upcoming courses -->

						</div>
					</div>
				</div>
							<!-- /detached sidebar -->

			</div>
    </div>
	</div>

@endsection
