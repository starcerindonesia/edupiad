@extends('layouts.MasterDasboard')

@section('content')

<!-- Page header -->
	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Pendaftaran </span>Program</h4>
				<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

				<div class="heading-elements">
					<div class="heading-btn-group">
						<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
						<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
						<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
					</div>
				</div>
			</div>

			<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
				<ul class="breadcrumb">
					<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
					<li class="active">Pendaftaran Program</li>
				</ul>

				<ul class="breadcrumb-elements">
					<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-gear position-left"></i>
							Settings
							<span class="caret"></span>
						</a>

						<ul class="dropdown-menu dropdown-menu-right">
							<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
							<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
							<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
							<li class="divider"></li>
							<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<!-- /page header -->


		<!-- Content area -->
		<div class="content">

			<!-- Floating labels -->
			<div class="row">
				<div class="col-md-12">
					<!-- Text inputs -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Pendaftaran Program<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
							<div class="heading-elements">
								<ul class="icons-list">
									<li><a data-action="collapse"></a></li>
									<li><a data-action="reload"></a></li>
									<li><a data-action="close"></a></li>
								</ul>
							</div>
						</div>

						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<div class="input-group" id="adv-search">
										<input type="text" class="form-control" placeholder="Pencarian Program" />
										<div class="input-group-btn">
											<div class="btn-group" role="group">
												<div class="dropdown dropdown-lg">
													<button data-action="reload" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
												</div>
											</div>
										</div>
									</div>
								</div>
	
					</div>
				</div>
			</div>
		</div>
					<!-- Detached content -->
					<div class="container-detached">
						<div class="content-detached">

							<!-- List -->
							<div class="row" >
								@foreach($Program as $no=>$prog)
								<div class="col-lg-6">
									<div class="panel panel-flat blog-horizontal blog-horizontal-2">
										<div class="panel-body">
											<div class="thumb">
												<a href="{{route('PendaftaranProgram.show',$prog->id_program)}}" data-toggle="modal">
													<img src="/img/gambar_program/{{$prog->gambar}}" class="img-responsive img-rounded" alt="" >
													<span class="zoom-image"><i class="icon-eye"></i></span>
												</a>
											</div>

											<div class="blog-preview">
												<div class="content-group-sm media blog-title stack-media-on-mobile text-left">
													<div class="media-body">
														<h5 class="text-semibold no-margin"><a href="{{route('PendaftaranProgram.show',$prog->id_program)}}" class="text-default">{{$prog->nama_program}}</a></h5>

														<ul class="list-inline list-inline-separate  text-muted">
															<li>{!! date('d-F-Y',strtotime($prog->tanggal_awal)) !!} S.D {!! date('d-F-Y',strtotime($prog->tanggal_akhir)) !!}</li>
														</ul>
													</div>


												</div>

												<p>Tempat : <br>{!! substr($prog->tempat_kegiatan,0,40) !!} <a href="{{route('PendaftaranProgram.show',$prog->id_program)}}">[...]</a>
											</div>
										</div>

										<div class="panel-footer panel-footer-condensed"><a class="heading-elements-toggle"><i class="icon-more"></i></a>
											<div class="heading-elements">
												<ul class="list-inline list-inline-separate heading-text">
													<li><i class="icon-users position-left text-primary"></i> 382</li>
													<li class="text-danger"><i class="icon-calendar52 position-left text-danger"></i> {!! date('d/m/Y',strtotime($prog->batas_pendaftaran)) !!}  </li>
													<li><i class="icon-coins position-left text-warning"></i>Rp.{{$prog->harga}}/Peserta
													</li>
												</ul>
												<a   href="{{route('PendaftaranProgram.show',$prog->id_program)}}" type="button" class="btn btn-danger btn-rounded  pull-right"><i class="icon-help position-left"></i> Detail<i class="icon-arrow-right14 position-right"></i></a>
											</div>
										</div>
									</div>
								</div>
								@endforeach
							</div>
							<!-- /list -->


							<!-- Pagination -->
							<div class="text-center content-group-lg pt-20">
								<ul class="pagination">
									<li class="disabled"><a href="#"><i class="icon-arrow-small-left"></i></a></li>
									<li class="active"><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#">4</a></li>
									<li><a href="#">5</a></li>
									<li><a href="#"><i class="icon-arrow-small-right"></i></a></li>
								</ul>
							</div>
							<!-- /pagination -->

						</div>
					</div>
				</div>

					@endsection
