<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if (Auth::user()->keterangan == 'admin') {
		return redirect('admin');
	}else{
		return redirect('home');
	}
	})->middleware(['auth','verified']);

// Untuk Middleware jangan di hilangin untuk Security Login
Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware(['auth','verified']);
Route::get('/pembayaran', 'PembayaranController@getPembayaran')->name('pembayaran')->middleware(['auth','verified']);

//<<<<<<< HEAD
Route::get('/afterVerified', function () {
	return view('afterVerified');
})->middleware(['auth','verified']);

Route::get('/oauth', function () {
	return view('oauth.index');
});


Route::get('/download', function () {
	return view('download.FormDownload');
});
Route::get('/UserProfile', function () {
	return view('UserProfile');
});


Route::resource('PendaftaranProgram', 'ProgramController')->middleware(['auth','verified']);



Route::resource('user', 'UserController')->middleware(['auth','verified']);




Route::get('/tes', function () {

	print_r(session()->all());
});


// Halaman Admin
Route::get('/admin', 'AdminController@index')->name('admin')->middleware(['auth','verified'],'admin');
