<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $kategori = Auth::user()->kategori;
        $email = Auth::user()->email;
                // if ($kategori == "") {
                   // return redirect('afterVerified')->with('email', $email);;
                // }else{
                   return view('home');
                // }
    }
}
