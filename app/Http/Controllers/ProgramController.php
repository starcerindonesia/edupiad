<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Program;
use App\User;
use App\Data_peserta;

class ProgramController extends Controller
{


    public function index()
  {
      $data = Program::all();
      return view('PendaftaranProgram.FormPendaftaranProgram',['Program'=> $data]);

  }

    public function show($id)
    {
      $program = Program::find($id);
      $action = "Detail";

      $data_peserta= Data_peserta::
      selectRaw('data_peserta.id_program , data_peserta.id_user, users.name, users.kategori, users.foto')
      ->Join('users', 'data_peserta.id_user','=','users.id')
      ->where('data_peserta.id_program','=',$id)
      ->orderBy('users.name', 'ASC')
      ->get();

      $pendaftar = Data_peserta::
			selectRaw('data_peserta.created_at,data_peserta.id_user, users.name,  users.kategori, users.foto')
      ->Join('users', 'data_peserta.id_user','=','users.id')
      ->where('data_peserta.id_program','=',$id)
			->orderBy('created_at', 'DESC')
			->get();


      $kategori = Auth::user()->kategori;

      return view('PendaftaranProgram.DetailPendaftaranProgram', compact('program','data_peserta','pendaftar','action','kategori'));
    }
}
