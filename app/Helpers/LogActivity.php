<?php


namespace App\Helpers;
use Request;
use App\LogActivityModel as LogActivityModel;


class LogActivity
{
    protected $table = 'users_log';


    public static function addToLog($subject)
    {
    	$log = [];
    	$log['subject'] = $subject;
    	$log['url'] = Request::fullUrl();
    	$log['method'] = Request::method();
    	$log['ip'] = Request::ip();
    	$log['agent'] = Request::header('user-agent');
    	$log['id_user'] = auth()->check() ? auth()->user()->id_user : 1;
        LogActivityModel::create($log);
    }


    public static function logActivityLists()
    {
    	return LogActivityModel::latest()->get();
    }


    public static function logActivityLogin()
    {
        return LogActivityModel::select(
        'id_user',
        'nama',
        'username',
        'nip',
        'level',
        'subject',
        'ip',
        'agent',
        'users_log.created_at' 
        )
        ->leftJoin('users', 'users_log.id_user','=','users.id_user')
        ->where('users_log.subject', '=', "Log In")
        ->orderBy('users_log.created_at', 'desc')
        ->get();
    }

}