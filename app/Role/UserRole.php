<?php

namespace App\Role;


class UserRole 
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_USER = 'ROLE_USER';
    const ROLE_GURU = 'ROLE_GURU';
    const ROLE_SISWA = 'ROLE_SISWA';

    /**
     * @var array
     */
    protected static $roleHierarchy = [
        self::ROLE_ADMIN => ['*'],
        self::ROLE_USER => [
            self::ROLE_SISWA,
            self::ROLE_GURU,
        ]
    ];

    /**
     * @param string $role
     * @return array
     */
    public static function getAllowedRoles(string $role)
    {
        if (isset(self::$roleHierarchy[$role])) {
            return self::$roleHierarchy[$role];
        }

        return [];
    }

    /***
     * @return array
     */
    public static function getRoleList()
    {
        return [
            static::ROLE_ADMIN =>'Admin',
            static::ROLE_USER => 'User',
            static::ROLE_SISWA => 'Siswa',
            static::ROLE_GURU => 'Guru',
        ];
    }
}
