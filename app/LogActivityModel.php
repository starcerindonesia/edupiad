<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogActivityModel extends Model
{
    protected $table = 'users_log';
    
    protected $fillable = [
        'subject', 'url', 'method', 'ip', 'agent', 'id_user'
    ];
}
