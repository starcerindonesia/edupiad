<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data_peserta extends Model
{
  protected $table = 'data_peserta';
	protected $primaryKey = 'id_program';
	protected $fillable = ['id_program','id_siswa'];
	public $timestamps = true;
}
