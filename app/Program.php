<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
  protected $table = 'program';
	protected $primaryKey = 'id_program';
	protected $fillable = ['kode_program','gambar','nama_program','syarat','tempat_kegiatan','harga','pemateri','batas_pendaftaran','tanggal_awal','tanggal_akhir'];
	public $timestamps = true;
}
